## ---------------------------
##
## Script name: database_MP.R
##
## Purpose of script: Prepare files to construct a proteogenomics database
##
## Author: Maria Mu�oz Benavent
##
## Date Created: March 2019
##
## Copyright (c) <Name>, 2018
## Email: Maria.Munoz-Benavent@uv.es
##
## ---------------------------
##
## Notes:
##   
##
## ---------------------------

# Load packages (installed using conda)
library(Biostrings)
library(stringr)

# Get arguments from commandline
args = commandArgs(trailingOnly=TRUE)

# Set working directory
print(paste(args[1],"MP/database",sep=""))
setwd(paste(args[1],"MP/database",sep=""))

# List files in temp
temp<-list.files(pattern = "*ProtOK.fasta")
print(temp)

# Get number of samples
nsamples<-1:length(temp)
numer <- str_pad(nsamples,2,pad = "0")

# Make FASTA files readable
for (i in 1:length(temp)){ 
  fasta_name<-paste("sample",numer,sep="")
  assign(fasta_name[i], readAAStringSet(temp[i]))
}

# Name each protein identified in the samples with non repetitive names
for(i in 1:length(fasta_name)) { 
  names(fasta_name[i]) <- paste("sample",i, sep = "_")
}

# Write to a file
for (i in 1:length(fasta_name)){ 
  csvfile<-paste0("Prot",i,".fasta")
  writeXStringSet(get(fasta_name[i]), csvfile)
}


